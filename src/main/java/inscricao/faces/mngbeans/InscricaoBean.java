package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import inscricao.entity.Idioma;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@Named
@RequestScoped
public class InscricaoBean extends PageBean {
    
    @Inject
    private RegistroBean candidatosList;
    
    private DataModel<Candidato> candidadtosListDataModel;
    
    private static final Idioma[] IDIOMAS = {
        new Idioma(1, "Inglês"),
        new Idioma(2, "Alemão"),
        new Idioma(3, "Francês")
    };
    
    private Candidato candidato = new Candidato(IDIOMAS[0]); // inicialmente ingles
    private List<SelectItem> idiomaItemList;
    private ArrayDataModel<Idioma> idiomasDataModel;

    public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    public Idioma[] getIdiomas() {
        return IDIOMAS;
    }
    
    public List<SelectItem> getIdiomaItemList() {
        if (idiomaItemList != null) return idiomaItemList;
        idiomaItemList = new ArrayList<>();
        for (Idioma id: IDIOMAS) {
            idiomaItemList.add(new SelectItem(id.getCodigo(), id.getDescricao()));
        }
        return idiomaItemList;
    }
    
    public ArrayDataModel<Idioma> getIdiomasDataModel() {
        if (idiomasDataModel == null) {
            idiomasDataModel = new ArrayDataModel<>(IDIOMAS);
        }
        return idiomasDataModel;
    }

    public RegistroBean getCandidatosList() {
        return candidatosList;
    }

    public void setCandidatosList(RegistroBean candidatos) {
        this.candidatosList = candidatos;
    }
    
    public String confirmaAction() {
        candidato.setDataHora(new Date());
        candidato.setIdioma(IDIOMAS[candidato.getIdioma().getCodigo()-1]);
        candidatosList.addCandidatosList(candidato);
        
        return "confirma";
    }
    
    public DataModel<Candidato> getCandidatosListDataModel() {
        candidadtosListDataModel = new ListDataModel<>(candidatosList.getCandidatosList());
        return candidadtosListDataModel;
    }
    
    public String candidatoAction(){
        setCandidato(candidadtosListDataModel.getRowData());
        candidatosList.getCandidatosList().remove(candidadtosListDataModel.getRowData());
        return "inscricao";
    }
    
    public void excluiAction(){
        candidatosList.getCandidatosList().remove(candidadtosListDataModel.getRowData());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import utfpr.faces.support.PageBean;
/**
 *
 * @author andre
 */
@Named
@ApplicationScoped
public class RegistroBean extends PageBean{
    private ArrayList<Candidato> CandidatosList = new ArrayList<>();

    public ArrayList<Candidato> getCandidatosList() {
        return CandidatosList;
    }

    public void addCandidatosList(Candidato candidato) {
        this.CandidatosList.add(candidato);
    }
    
    
}

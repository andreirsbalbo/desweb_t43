/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.validate;

import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author andre
 */
@FacesValidator("cpfValidator")
public class CPFValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object value) throws ValidatorException {
        String cpf = value.toString();
        
        
        if (!Pattern.matches("^\\d{11}|\\d{9}-\\d{2}|\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$", cpf)) {
            FacesMessage msg =
                new FacesMessage("CPF 'valor digitado' inválido");
            throw new ValidatorException(msg);
        }
    }
    
}
